from django.urls import path
from . import views
#the urls of the app
urlpatterns = [
    path('', views.home, name = "home"),
    path('product/',views.product, name = "product"),
    path('Business/',views.business, name = "business"),
    path('login/',views.login_user, name = "login"),
    path('logout',views.logout_user, name = "logout"),
    path('cart/',views.cart, name = "cart"),
    path('signup/',views.register, name = "signup"), 
    path('checkout/',views.checkout, name ="checkout"),
    path('product',views.product, name ="product"),
    path('update_item/',views.updateItem, name = 'update_item'),
    path('process_order/',views.processOrder, name = 'process_order'),
    path('regproduct',views.regproduct, name='regproduct'),
    path('regbusiness',views.regbusiness, name='regbusiness'),
    path('search',views.search, name='search'),
    path('category/<str:foo>',views.category, name='category'),
    path('product/<int:pk>',views.productcard, name='productcard'),

    #path for user account page
    
]