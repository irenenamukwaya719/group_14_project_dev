from django.contrib.auth.models import User
from django.db import models



# Create your models here.
class Customer(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE, null = True, blank = True)
    name = models.CharField(max_length= 200, null = True)
    phone = models.CharField(max_length= 200, null = True)
    email = models.CharField(max_length= 200, null = True)
    date_created = models.DateTimeField(auto_now_add = True, null = True)

    def __str__(self):
        return self.name
    
# Each product must belong to a category and the category table of the database only takes the category name.
class Category(models.Model):
    name=models.CharField(max_length=50)

    def __str__(self):
         return self.name
     

# This a database table  called Product that stores product details as listed below.
#     A product must have all the details below and not miss out any. 
    
class Product(models.Model):
    name= models.CharField(max_length=100)
    price= models.FloatField(null = True)
    category= models.ForeignKey(Category,on_delete=models.CASCADE,default=1)
    digital = models.BooleanField(default = False, null = True, blank= False)
    picture= models.ImageField(upload_to='uploads/product/',default=1)
    description= models.TextField(max_length =500)

    


    def __str__(self):
        return self.name
    
    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url
    

# This is a database table that stores business details when the business is being registered.
    #Each business must provide the details listed below.
class Business(models.Model):
    name= models.CharField(max_length=100)
    location=models.CharField(max_length=100)
    email = models.EmailField()
    contacts=models.CharField(max_length=20)
    description=models.TextField(max_length =500)
    logo = models.ImageField(upload_to='business_logos/', blank=True, null=True)#Only the logo is optional.
   

#This is what shows in the database. The name and the location.
    def __str__(self):
        return self.name + " " + self.location


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete = models.SET_NULL, null = True, blank= True)
    
    date_ordered = models.DateTimeField(auto_now_add = True, null = True)
    complete = models.BooleanField(default=False, null = True, blank = False)
    transaction_id = models.CharField(max_length = 100 ,null = True)

    def __str__(self):
        return  str(self.customer) +  str(' order No. ')+ str(self.id)
        
    @property
    def shipping(self):
        shipping = False
        orderitems = self.orderitem_set.all()
        for i in orderitems:
            if i.product.digital == False:
                shipping = True
        return shipping
    
    @property
    def get_cart_total(self):
       orderitems = self.orderitem_set.all()
       total = sum([item.get_total for item in orderitems])
       return total

    @property
    def get_cart_items(self):
       orderitems = self.orderitem_set.all()
       total_items = sum(item.get_quantity for item in orderitems)
       return total_items
       
class OrderItem(models.Model):
    product = models.ForeignKey(Product, null= True, on_delete = models.SET_NULL, blank = True)
    order = models.ForeignKey(Order, null= True, on_delete = models.SET_NULL)
    quantity = models.IntegerField(default= 0, null = True, blank = True)
    date_added = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.product.name
    
    @property
    def get_total(self):
        total = self.product.price * self.quantity
        return total
    
    @property
    def get_quantity(self):
        total_quantity = self.quantity
        return total_quantity
    
    

class ShippingAddress(models.Model):
    customer = models.ForeignKey(Product, null= True, on_delete = models.SET_NULL, blank = True)
    order = models.ForeignKey(Order, null= True, on_delete = models.SET_NULL)
    address = models.CharField(max_length = 100 ,null = True)
    city = models.CharField(max_length = 100 ,null = True)
    state = models.CharField(max_length = 100 ,null = True)
    zipcode = models.CharField(max_length = 100 ,null = True)
    date_added = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.address



