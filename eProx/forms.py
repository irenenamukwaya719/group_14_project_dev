from django.forms import ModelForm
from .models import *
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm



#This is python form that is to register new users in the database.
class SignUpForm(UserCreationForm):
	
	class Meta:
		model = User
		fields = ('username', 'password1', 'password2') 	# username is the username of the new user.
            												#password1 is the first password the user enters.
															# password2 is the confirmation of the first password entered by the user


#These are just widegts for the signup form containig mostly placeholders and the bootstrap class "form-control"
	def __init__(self, *args, **kwargs):
		super(SignUpForm, self).__init__(*args, **kwargs)

		self.fields['username'].widget.attrs['class'] = 'form-control'
		self.fields['username'].widget.attrs['placeholder'] = 'User Name'
		self.fields['username'].label = ''
	

		self.fields['password1'].widget.attrs['class'] = 'form-control'
		self.fields['password1'].widget.attrs['placeholder'] = 'Password'
		self.fields['password1'].label = ''
	

		self.fields['password2'].widget.attrs['class'] = 'form-control'
		self.fields['password2'].widget.attrs['placeholder'] = 'Confirm Password'
		self.fields['password2'].label = ''
		        
    

#The product form is for registering  new products onto the website.
class Productform(ModelForm):
    class Meta:
        model=Product
        fields="__all__"# This extracts all the fields of the product model.
        

#The business form is for registering  new businesses onto the website.
class Businessform(ModelForm):
    class Meta:
        model =Business
        fields="__all__" # This extracts all the fields of the Buiness model.			
        