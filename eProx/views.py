from django.shortcuts import render,redirect
from .models import *
from django.contrib.auth import authenticate,login,logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms
from.forms import SignUpForm,Productform,Businessform
from django.http import JsonResponse
import json
import datetime



#This is the view for the home page.
def home(request):
    
    products = Product.objects.all()#This is a query expression in  that retrieves all the objects from the Product table in the database.
    context ={'products':products}
    return render(request, 'eProx/homepage.html',context)


#This is a view for the product page which shows all products available in the database.
def product(request):
    products = Product.objects.all()  #This is a query expression in  that retrieves all the objects from the Product table in the database.
    return render(request,'eProx/product.html',{'products':products})



#Each product is assigned a product card for organisation and easy viewing .
def productcard(request,pk):
    product= Product.objects.get(id=pk) #This retrieves a specific instance of the Product model from the database based on its primary key used as a product id.
    return render(request,'eProx/productcard.html',{'product':product})



def business(request):
    context = {}
    return render(request, 'eProx/business.html', context)

 

def category(request,foo):
    #just replacing hyphens with spaces...nothing much
    foo = foo.replace('-',' ')
    try:
        category=Category.objects.get(name=foo)
        products= Product.objects.filter(category=category)
        return render(request,'eProx/category.html',{'products':products, 'category':category})
    except:
        return redirect('home')


def login_user(request):
    if request.method =="POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        
        #this checks if the user is already registered in the database or not.
        #if they are already registered, they will be logged in. 
        if user is not None:
            login(request, user)
            return redirect ('home')# takes user back to home page after logging in
        #if not.....they are not logged in.
        else:
            return redirect ('login')
            
    
    else:
        return render(request,'eProx/login.html',{})
    

# This is for product registration.
def regproduct(request):
    form = Productform()
    if request.method == "POST":
        form= Productform(request.POST,request.FILES)
        if form.is_valid():#the product information is only saved in the database if it is valid as per the product model.
            form.save()
            return redirect ('product')# once the product information is valid and saved in the database,the user is re-directed 
          
        else:
            return render(request,'eProx/productreg.html',{'form':form})
    else:           
        return render(request,'eProx/productreg.html',{'form':form})
    
# The regbusiness view is for registering new businesses.
def regbusiness(request):
    form = Businessform()
    if request.method =="POST":
        form= Businessform(request.POST,request.FILES)
        if form.is_valid(): #The business information must be valid for it to be saved in the database
            form.save()
            return redirect ('home')
        else:
            return render(request,'eProx/regbusiness.html',{'form':form})
    else:           
        return render(request,'eProx/regbusiness.html',{'form':form}) 


# This is the search view
def search(request):
    if request.method =='POST':
        searched= request.POST['searched']
        #now query the database
        searched= Product.objects.filter(name__icontains=searched) # we use icontains because its not case sensitive
        return render(request,'eProx/search.html',{'searched':searched})
    else:
        return render(request,'eProx/search.html',{})       

    
#This logs the current user.
def logout_user(request):
    logout(request)
    return redirect('home')    
    
    
def cart(request):
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set. all()
    else:
        items = []
        order = {'get_cart_total':0, 'get_cart_items':0}

    context = {'items':items, 'order':order}
    return render(request, 'eProx/cart.html', context)

# The register view is for signing up new users.
def register(request):
    form = SignUpForm()
    if request.method =="POST":
        form= SignUpForm(request.POST)
        if form.is_valid(): #The user information must be valid for it to be saved in the database
            form.save() 
     
            username= form.cleaned_data.get('username')   #This extracts the cleaned and validated value of the 'username' field from the form
            password= form.cleaned_data.get('password1')  #This extracts the cleaned and validated value of the 'password1' field from the form
            user = authenticate(request,username=username,password=password)
            if user is not None: #once the user is fully registered and saved in the database, they are now authenticated and logged in
                login(request,user)
                return redirect ('home')
        else: 
            return redirect ('home')
    else:           
        return render(request,'eProx/signup.html',{'form':form})
    


def checkout(request):
       
    if request.user.is_authenticated:
          customer =request.user.customer
          order,created = Order.objects.get_or_create(customer, complete =False)
          items = order.orderitem_set. all()
    else:
          items =[]
          order = {'get_cart_total':0, 'get_cart_items':0}
          
    context ={'items':items, 'order':order}


    return render(request,'eProx/checkout.html',context)


def updateItem(request):
	data = json.loads(request.body)
	productId = data['productId']
	action = data['action']
	print('Action:', action)
	print('Product:', productId)

	customer = request.user.customer
	product = Product.objects.get(id=productId)
	order , created = Order.objects.get_or_create(customer=customer, complete=False)
	orderItem, created = OrderItem.objects.get_or_create(order=order, product=product)

	if action == 'add':
		orderItem.quantity = (orderItem.quantity + 1)
	elif action == 'remove':
		orderItem.quantity = (orderItem.quantity - 1)

	orderItem.save()

	if orderItem.quantity <= 0:
		orderItem.delete()

	return JsonResponse('Item was added', safe=False)



def  processOrder(request):
     transaction_id = datetime.datetime.now().timestamp()
     data = json.loads(request.body)

     if request.user.is_authenticated:
        customer = request.user.customer
        order,created = Order.objects.get_or_create(customer=customer, complete=False)
        total = float(data['form']['total'])
        order.transaction_id = transaction_id
        if total == order.get_cart_total:
            order.complete = True
        order.save
        if order.shipping == True:
             ShippingAddress.objects.create(
                  customer=customer,
                  order=order,
                  address=data["shipping"]["address"],
                  city=data["shipping"]["city"],
                  state=data["shipping"]["state"],
                  zipcode=data["shipping"]["zipcode"],
             )
     else:
        print('User is not logged in')
     return JsonResponse('Payment Complete', safe=False)

